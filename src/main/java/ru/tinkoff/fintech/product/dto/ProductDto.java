package ru.tinkoff.fintech.product.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class ProductDto {

    private Long id;

    private String title;

    private BigDecimal price;

}
